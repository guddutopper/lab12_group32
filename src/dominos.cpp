 /*
* Copyright (c) 2006-2009 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

/* 
 * Base code for CS 251 Software Systems Lab 
 * Department of Computer Science and Engineering, IIT Bombay
 * 
 */
 
 


#include "cs251_base.hpp"
#include "render.hpp"

#ifdef __APPLE__
        #include <GLUT/glut.h>
#else
        #include "GL/freeglut.h"
#endif

#include <cstring>
#include <math.h>
using namespace std;

#include "dominos.hpp"

namespace cs251
{
  /**  The is the constructor 
   * This is the documentation block for the constructor.
   */ 
  
  dominos_t::dominos_t()
  {
	   /// # Initialization of Simulation
    
    /*! 
     s is a variable of type b2Body* and points to
     * the circle which falls from up and start the simulation.
     * Radius of the circle is 0.5f.
     */ 
    
   
    
         {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-36.9f, 45.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }    
 
   
     {
		  /// # Ball Bearing Structure
    
   /*! 
     This is one of the main part of the simulation.
    * In this structure we made four storey structure in which the ball simulates and goes down. 
    * They are following:
    */
    
    
    /// ## First part
     /*! 
     This is the upper part of ball bearing structure 
     * which starts simulting after falling of the initial ball 
     * from upward and subsequently flow through second 
     * and third part of ball bearing structure.
    */
      b2PolygonShape shape;
      shape.SetAsBox(3.5f, 0.1f);
        
      b2BodyDef bd2;
      bd2.position.Set(-33.0f, 38.25f);
      bd2.angle=0.35;
      b2Body* ground = m_world->CreateBody(&bd2);
      ground->CreateFixture(&shape, 0.0f);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(1.5f, 0.1f);
        
      b2BodyDef bd2;
      bd2.position.Set(-30.75f, 40.5f);
      bd2.angle=-0.35;
      b2Body* ground = m_world->CreateBody(&bd2);
      ground->CreateFixture(&shape, 0.0f);
    }
     {
      b2PolygonShape shape;
      shape.SetAsBox(5.5f, 0.1f);
        
      b2BodyDef bd2;
      bd2.position.Set(-30.5f, 37.25f);
      bd2.angle=0.35;
      b2Body* ground = m_world->CreateBody(&bd2);
      ground->CreateFixture(&shape, 0.0f);
    }
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(3.0f, 0.1f);
        
      b2BodyDef bd;
      bd.position.Set(-38.0f, 40.5f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 1.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 0.1;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(0.1,0.7, b2Vec2(-3.0f,0.7f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 0.1;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(1.5,0.1, b2Vec2(-1.5f,1.4f), 0);
      fd2->shape = &bs2;
      body->CreateFixture(fd);
      body->CreateFixture(fd1);
      body->CreateFixture(fd2);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.1f, 0.1f);
      b2BodyDef bd2;
      bd2.position.Set(-38.65f, 40.5f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(-0.65,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }

       {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-37.25f, 39.5f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
        {
      b2PolygonShape shape;
      shape.SetAsBox(2.125f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-34.325f, 41.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
         {
      b2PolygonShape shape;
      shape.SetAsBox(3.0f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-39.75f, 37.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
     {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-38.5f, 41.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-37.5f, 41.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-39.5f, 41.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-40.5f, 41.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    
    
    
          {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.5f);
        
      b2BodyDef bd;
      bd.position.Set(-37.75f, 35.5f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    /// ## Second part
     /*! 
     This is the second part of ball bearing structure 
     * which starts simulting after falling of the ball 
     * from upper part and subsequently flow through third part of ball bearing structure.
    */
    
    
    
    
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(3.5f, 0.1f);
        
      b2BodyDef bd2;
      bd2.position.Set(-33.0f, 31.25f);
      bd2.angle=0.35;
      b2Body* ground = m_world->CreateBody(&bd2);
      ground->CreateFixture(&shape, 0.0f);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(1.5f, 0.1f);
        
      b2BodyDef bd2;
      bd2.position.Set(-30.75f, 33.5f);
      bd2.angle=-0.35;
      b2Body* ground = m_world->CreateBody(&bd2);
      ground->CreateFixture(&shape, 0.0f);
    }
     {
      b2PolygonShape shape;
      shape.SetAsBox(5.5f, 0.1f);
        
      b2BodyDef bd2;
      bd2.position.Set(-30.5f, 30.25f);
      bd2.angle=0.35;
      b2Body* ground = m_world->CreateBody(&bd2);
      ground->CreateFixture(&shape, 0.0f);
    }
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(3.0f, 0.1f);
        
      b2BodyDef bd;
      bd.position.Set(-38.0f, 33.5f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 1.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 0.1;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(0.1,0.7, b2Vec2(-3.0f,0.7f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 0.1;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(1.5,0.1, b2Vec2(-1.5f,1.4f), 0);
      fd2->shape = &bs2;
      body->CreateFixture(fd);
      body->CreateFixture(fd1);
      body->CreateFixture(fd2);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.1f, 0.1f);
      b2BodyDef bd2;
      bd2.position.Set(-38.65f, 33.5f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(-0.65,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }

       {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-37.25f, 32.5f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
        {
      b2PolygonShape shape;
      shape.SetAsBox(2.125f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-34.325f, 34.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
         {
      b2PolygonShape shape;
      shape.SetAsBox(3.0f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-39.75f, 30.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
     {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-38.5f, 34.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-37.5f, 34.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-39.5f, 34.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-40.5f, 34.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    /// ## Third part
     /*! 
     This is the lower and last part of ball bearing structure 
     * which starts simulting after falling of the balls 
     * from second part and subsequently flow to last part of ball bearing structure.
    */
    
    
    

          {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.5f);
        
      b2BodyDef bd;
      bd.position.Set(-37.75f, 28.5f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    
    
    
    
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(3.5f, 0.1f);
        
      b2BodyDef bd2;
      bd2.position.Set(-33.0f, 24.25f);
      bd2.angle=0.35;
      b2Body* ground = m_world->CreateBody(&bd2);
      ground->CreateFixture(&shape, 0.0f);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(1.5f, 0.1f);
        
      b2BodyDef bd2;
      bd2.position.Set(-30.75f, 26.5f);
      bd2.angle=-0.35;
      b2Body* ground = m_world->CreateBody(&bd2);
      ground->CreateFixture(&shape, 0.0f);
    }
     {
      b2PolygonShape shape;
      shape.SetAsBox(5.5f, 0.1f);
        
      b2BodyDef bd2;
      bd2.position.Set(-30.5f, 23.25f);
      bd2.angle=0.35;
      b2Body* ground = m_world->CreateBody(&bd2);
      ground->CreateFixture(&shape, 0.0f);
    }
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(3.0f, 0.1f);
        
      b2BodyDef bd;
      bd.position.Set(-38.0f, 26.5f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 1.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 0.1;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(0.1,0.7, b2Vec2(-3.0f,0.7f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 0.1;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(1.5,0.1, b2Vec2(-1.5f,1.4f), 0);
      fd2->shape = &bs2;
      body->CreateFixture(fd);
      body->CreateFixture(fd1);
      body->CreateFixture(fd2);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.1f, 0.1f);
      b2BodyDef bd2;
      bd2.position.Set(-38.65f, 26.5f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(-0.65,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }

       {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-37.25f, 25.5f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
        {
      b2PolygonShape shape;
      shape.SetAsBox(2.125f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-34.325f, 27.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
         {
      b2PolygonShape shape;
      shape.SetAsBox(3.0f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-39.75f, 23.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
     {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-38.5f, 27.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-37.5f, 27.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-39.5f, 27.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-40.5f, 27.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    /// ## Forth part

  /*! 
     This is the lower part of ball bearing structure 
     * which starts simulting after falling of the  ball 
     * from third part and then ball falls in open box.
    */
    
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(3.5f, 0.1f);
        
      b2BodyDef bd2;
      bd2.position.Set(-33.0f, 17.25f);
      bd2.angle=0.35;
      b2Body* ground = m_world->CreateBody(&bd2);
      ground->CreateFixture(&shape, 0.0f);
    }
    
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(3.0f, 0.1f);
        
      b2BodyDef bd;
      bd.position.Set(-38.0f, 19.5f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 1.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 0.1;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(0.1,0.7, b2Vec2(-3.0f,0.7f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 0.1;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(1.5,0.1, b2Vec2(-1.5f,1.4f), 0);
      fd2->shape = &bs2;
      body->CreateFixture(fd);
      body->CreateFixture(fd1);
      body->CreateFixture(fd2);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.1f, 0.1f);
      b2BodyDef bd2;
      bd2.position.Set(-38.65f, 19.5f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(-0.65,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }

       {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-37.25f, 18.5f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
        {
      b2PolygonShape shape;
      shape.SetAsBox(2.125f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-34.325f, 20.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
         {
      b2PolygonShape shape;
      shape.SetAsBox(2.5f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-39.25f, 16.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
     {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-38.5f, 20.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-37.5f, 20.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-39.5f, 20.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-40.5f, 20.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    
    /// ### Left line of Ball Bearing 
    /*! 
     This is the left straight line in the Ball bearing structure 
     * which prevents the falling balls from going outside of the open box
     * which is at the left side of see-saw . 
    */
 

      {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 12.0f);
        
      b2BodyDef bd;
      bd.position.Set(-44.5f, 27.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    } 
    /// # Pulley System
   
 /*! 
     This is the dynamic pulley system which starts simulating after
     * the left see-saw edge touches the horizontal shelf with a ball and then 
     * the ball falls in the open container of the pullry system and
     * then the pulley goes down and other side of pulley goes up and touches 
     * the other horizontal shelf with ball and the simulation continues.
    */
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(10.0f, 35.8f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    } 
    
    //The pulley system
    {
      b2BodyDef *bd = new b2BodyDef;
      bd->type = b2_dynamicBody;
      bd->position.Set(-23,8);
      bd->fixedRotation = true;
      
      //The open box
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 10.0;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(2,0.2, b2Vec2(0.f,-1.9f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 10.0;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(0.2,2, b2Vec2(2.0f,0.f), 0);
      fd2->shape = &bs2;
      b2FixtureDef *fd3 = new b2FixtureDef;
      fd3->density = 10.0;
      fd3->friction = 0.5;
      fd3->restitution = 0.f;
      fd3->shape = new b2PolygonShape;
      b2PolygonShape bs3;
      bs3.SetAsBox(0.2,2, b2Vec2(-2.0f,0.f), 0);
      fd3->shape = &bs3;
       
      b2Body* box1 = m_world->CreateBody(bd);
      box1->CreateFixture(fd1);
      box1->CreateFixture(fd2);
      box1->CreateFixture(fd3);

      //The bar
      bd->position.Set(10,38);        
      fd1->density = 34.0;          
      b2Body* box2 = m_world->CreateBody(bd);
      box2->CreateFixture(fd1);

      // The pulley joint
      b2PulleyJointDef* myjoint = new b2PulleyJointDef();
      b2Vec2 worldAnchorOnBody1(-23, 8); // Anchor point on body 1 in world axis
      b2Vec2 worldAnchorOnBody2(10, 38); // Anchor point on body 2 in world axis
      b2Vec2 worldAnchorGround1(-23, 43); // Anchor point for ground 1 in world axis
      b2Vec2 worldAnchorGround2(10, 43); // Anchor point for ground 2 in world axis
      float32 ratio = 1.0f; // Define ratio
      myjoint->Initialize(box1, box2, worldAnchorGround1, worldAnchorGround2,
box1->GetWorldCenter(), box2->GetWorldCenter(), ratio);
      m_world->CreateJoint(myjoint);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(4.0f, 0.2f);
        
      b2BodyDef bd;
      bd.position.Set(15.0f, 39.0f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 1.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.2f, 0.2f);
      b2BodyDef bd2;
      bd2.position.Set(15.0f, 41.0f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }

    //The sphere
    
    /// ### Sphere
     /*! 
     This is the sphere of radius 1.5f keeped on upper horizontal shelf.
    */
    
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 1.5;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 22.22f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(15.0f, 42.7f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    
    
    
    
    
   /// # Horizontal shelf with Dominos
    /*! 
     There are three Horizontal shapes of size 5.5f and width 0.25f 
     * with Dominos of size 0.5f and width 0.05.
    */
    



   
   
   
   // Right Top horizontal shelf
    {
      b2PolygonShape shape;
      shape.SetAsBox(5.5f, 0.25f);

      b2BodyDef bd;
      bd.position.Set(28.0f, 38.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
        {
      b2PolygonShape shape;
      shape.SetAsBox(2.0f, 0.25f);

      b2BodyDef bd;
      bd.position.Set(19.5f, 38.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
{
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.4;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 200.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(21.5f, 38.5f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    {
      b2BodyDef *bd = new b2BodyDef;
      bd->type = b2_staticBody;
      bd->position.Set(22.0,36);
      bd->fixedRotation = true;
      
      
      
      //The open box
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 10.0;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(1,0.05, b2Vec2(0.f,-0.95f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 10.0;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(0.05,1, b2Vec2(1.0f,0.f), 0);
      fd2->shape = &bs2;
      b2FixtureDef *fd3 = new b2FixtureDef;
      fd3->density = 10.0;
      fd3->friction = 0.5;
      fd3->restitution = 0.f;
      fd3->shape = new b2PolygonShape;
      b2PolygonShape bs3;
      bs3.SetAsBox(0.05,1, b2Vec2(-1.0f,0.f), 0);
      fd3->shape = &bs3;
       
      b2Body* box1 = m_world->CreateBody(bd);
      box1->CreateFixture(fd1);
      box1->CreateFixture(fd2);
      box1->CreateFixture(fd3);
}
{
      b2PolygonShape shape;
      shape.SetAsBox(0.5f, 0.05f);
      b2BodyDef bd;
      bd.position.Set(19.5f, 37.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
{
      b2PolygonShape shape;
      shape.SetAsBox(0.5f, 0.05f);
      b2BodyDef bd;
      bd.position.Set(19.5f, 36.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
{
      b2PolygonShape shape;
      shape.SetAsBox(0.5f, 0.05f);
      b2BodyDef bd;
      bd.position.Set(19.5f, 35.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
{
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 1.0f);
      b2BodyDef bd;
      bd.position.Set(20.0f, 36.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
{
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 1.0f);
      b2BodyDef bd;
      bd.position.Set(19.0f, 36.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
{
      b2PolygonShape shape;
      shape.SetAsBox(1.414f, 0.05f);

      b2BodyDef bd;
      bd.angle=0.7854;
      bd.position.Set(25.0f, 36.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
{
      b2PolygonShape shape;
      shape.SetAsBox(1.414f, 0.05f);

      b2BodyDef bd;
      bd.angle=-0.7854;
      bd.position.Set(25.0f, 36.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    

     // right Top Dominos
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 10.0f;
      fd.friction = 0.7f;

      for (int i = -1; i < 10; ++i)
  {
    b2BodyDef bd;
    bd.type = b2_dynamicBody;
    bd.position.Set(24.2f + 1.0f * i, 39.3f);
    b2Body* body = m_world->CreateBody(&bd);
    body->CreateFixture(&fd);
    
  }
    }
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(34.5f, 39.3f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(34.5f, 38.3f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,-1);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(34.0f, 35.7f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(34.0f, 35.7f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }    
      {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(34.5f, 34.0f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(34.5f, 34.0f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    } 


    // Right Second Top horizontal shelf
    {
      b2PolygonShape shape;
      shape.SetAsBox(8.0f, 0.25f);

      b2BodyDef bd;
      bd.position.Set(26.5f, 31.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
{
      b2PolygonShape shape;
      shape.SetAsBox(0.01f, 0.2f);

      b2BodyDef bd;
      bd.position.Set(34.2f, 31.5f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    


  // third horix 
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 10.0f;
      fd.friction = 0.7f;

      for (int i = -6; i < 10; ++i)
  {
    b2BodyDef bd;
    bd.type = b2_dynamicBody;
    bd.position.Set(25.0f + 1.0f * i, 32.3f);
    b2Body* body = m_world->CreateBody(&bd);
    body->CreateFixture(&fd);
  }
    }
    
    
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(17.7f, 32.3f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(17.7f, 31.3f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,-1);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }





    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(18.2f, 28.7f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(18.2f, 28.7f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }    
      {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(17.7f, 27.0f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(17.7f, 27.0f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }










    // Third  Right Top horizontal shelf
    {
      b2PolygonShape shape;
      shape.SetAsBox(8.0f, 0.25f);

      b2BodyDef bd;
      bd.position.Set(26.0f, 24.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }

  // Third Top Dominos
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 10.0f;
      fd.friction = 0.7f;

      for (int i = -6; i < 7; ++i)
  {
    b2BodyDef bd;
    bd.type = b2_dynamicBody;
    bd.position.Set(24.2f + 1.0f * i, 25.3f);
    b2Body* body = m_world->CreateBody(&bd);
    body->CreateFixture(&fd);
  }
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 1.0;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 5.0f;
      ballfd.friction = 0.1f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(32.2f, 25.3f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    
    
    
    
    /// # D-Shape in the Right side
    
     /*! 
     This is hte D-shape structure we made using Dominos and a vertical
     * line we simulate the dominos in a half circular structure such that
     * it make a D-shape.
    */
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 5.2f);

      b2BodyDef bd;
      bd.position.Set(37.1f, 31.1f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(36.1f, 39.3f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(36.1f, 40.3f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,1);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }
    
    
    for (int i = 0; i < 5; ++i)
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(37.1f + i*1.5f, 38.3f + sqrt(36-(i*i*1.5*1.5))*1.0f  - 6.0f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(37.1f + i*1.5f, 37.3f + sqrt(36-(i*i*1.5*1.5))*1.0f- 6.0f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,-1);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }
    for (int i = 0; i < 5; ++i)
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(37.1f + i*1.5f, 38.3f - sqrt(36-(i*i*1.5*1.5))*1.0f  - 8.4f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(37.1f + i*1.5f, 37.3f - sqrt(36-(i*i*1.5*1.5))*1.0f- 8.4f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,-1);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }

{
        
        
        float i=3.65;
        
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(37.1f + i*1.5f, 38.3f - sqrt(36-(i*i*1.5*1.5))*1.0f  - 8.4f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(37.1f + i*1.5f, 37.3f - sqrt(36-(i*i*1.5*1.5))*1.0f- 8.4f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,-1);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }
{
        
        
        float i=3.65;
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(37.1f + i*1.5f, 38.3f + sqrt(36-(i*i*1.5*1.5))*1.0f  - 6.0f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(37.1f + i*1.5f, 37.3f + sqrt(36-(i*i*1.5*1.5))*1.0f- 6.0f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,-1);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.5;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 5.0f;
      ballfd.friction = 0.1f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(37.6f, 21.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    
    
    
    
 /// ### Some Important shelfs
   /*! 
     These are some shelfs through which the ball simulates and continue 
     * the process and at last beat the box containing some eatable fluid
     * and then the fluid falls in the dog's box.
    */
     
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(4.0f, 0.2f);

      b2BodyDef bd;
      bd.position.Set(37.6f, 20.4f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(6.0f, 0.2f);

      b2BodyDef bd;
      bd.angle=0.09;
      bd.position.Set(37.6f, 18.4f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.2f, 1.0f);

      b2BodyDef bd;
      bd.position.Set(43.6f, 19.7f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(10.0f, 0.2f);

      b2BodyDef bd;
      bd.angle=0.09;
      bd.position.Set(23.2f, 19.4f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 2.5f);

      b2BodyDef bd;
      bd.position.Set(29.0f, 17.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(2.5f, 0.2f);
        
      b2BodyDef bd;
      bd.position.Set(28.0f, 13.0f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(28.0f, 13.0f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }
    
    
    /// # Container with Fluid
     /*! 
     This is the container containing eatable fluid for dog which falls after
     * hitted by a ball coming from upward and then the fluid falls in the do box.
    */
    
   
  
    //  box
    
    
    {
                b2BodyDef *bd = new b2BodyDef;
      bd->type = b2_dynamicBody;
      bd->position.Set(24,14);
      
      //The open box
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 0.01;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(2.1,0.1, b2Vec2(0.0f,0.0f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 0.01;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(0.1,2.1, b2Vec2(2.0f,2.0f), 0);
      fd2->shape = &bs2;
      b2FixtureDef *fd3 = new b2FixtureDef;
      fd3->density = 0.01;
      fd3->friction = 0.5;
      fd3->restitution = 0.f;
      fd3->shape = new b2PolygonShape;
      b2PolygonShape bs3;
      bs3.SetAsBox(0.1,2.1, b2Vec2(-2.0f,2.0f), 0);
      fd3->shape = &bs3;
       
      b2Body* box1 = m_world->CreateBody(bd);
      box1->CreateFixture(fd1);
      box1->CreateFixture(fd2);
      box1->CreateFixture(fd3);
                
                b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(24.0f, 14.0f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = box1;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
                
                }
                
   ///  # Fluids in the container
    /*! 
     This is the  eatable fluid for dog and is made up of 
     * small circles of radius 0.1f which falls to the dog's box 
     * after falling from the fluid container.
    */
                 
              
    for (int i = -7; i <= 8; ++i){
    for (int j = -8; j <= 8; ++j)
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.1;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.01f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(24.0f+i*0.2f, 16.0f+j*0.2f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    
}
   {
      b2PolygonShape shape;
      shape.SetAsBox(0.01f, 0.01f);

      b2BodyDef bd;
      bd.position.Set(26.2f, 17.5f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    } 
    {
      b2PolygonShape shape;
      shape.SetAsBox(10.0f, 0.2f);

      b2BodyDef bd;
      bd.angle=0.09;
      bd.position.Set(23.2f, 10.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(0.0f, 23.7f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    
    /// ## Another Pulley System
     /*! 
     This is second pulley which starts simulating after falling ball 
     * in the right side box of the pulley and this goes down and the upper 
     * side of puleey goes upward and continue the simulation by hitting the 
     * another ball.
     
    */
    
     //The second pulley system
    {
      b2BodyDef *bd = new b2BodyDef;
      bd->type = b2_dynamicBody;
      bd->position.Set(10,16);
      bd->fixedRotation = true;
      
      //The open box
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 10.0;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(2,0.2, b2Vec2(0.f,-1.9f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 10.0;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(0.2,2, b2Vec2(2.0f,0.f), 0);
      fd2->shape = &bs2;
      b2FixtureDef *fd3 = new b2FixtureDef;
      fd3->density = 10.0;
      fd3->friction = 0.5;
      fd3->restitution = 0.f;
      fd3->shape = new b2PolygonShape;
      b2PolygonShape bs3;
      bs3.SetAsBox(0.2,2, b2Vec2(-2.0f,0.f), 0);
      fd3->shape = &bs3;
       
      b2Body* box1 = m_world->CreateBody(bd);
      box1->CreateFixture(fd1);
      box1->CreateFixture(fd2);
      box1->CreateFixture(fd3);
      
      //The bar
      bd->position.Set(0,26);        
      fd1->density = 34.0;          
      b2Body* box2 = m_world->CreateBody(bd);
      box2->CreateFixture(fd1);

      // The pulley joint
      b2PulleyJointDef* myjoint = new b2PulleyJointDef();
      b2Vec2 worldAnchorOnBody1(10, 16); // Anchor point on body 1 in world axis
      b2Vec2 worldAnchorOnBody2(0, 26); // Anchor point on body 2 in world axis
      b2Vec2 worldAnchorGround1(10, 30); // Anchor point for ground 1 in world axis
      b2Vec2 worldAnchorGround2(0, 30); // Anchor point for ground 2 in world axis
      float32 ratio = 1.0f; // Define ratio
      myjoint->Initialize(box1, box2, worldAnchorGround1, worldAnchorGround2,
box1->GetWorldCenter(), box2->GetWorldCenter(), ratio);
      m_world->CreateJoint(myjoint);
    }
    
    
    
    ///  # Shelf With Ball
     /*! 
     This is a part of simulation and the shelf is 2.5f long 
     * and 0.2f wide.The sphere on the shelf is of radius 1.0f.
     * This continues the simulation and helps in feeding the dog.
    */
    
    
    //balance
    {
      b2PolygonShape shape;
      shape.SetAsBox(2.5f, 0.2f);
        
      b2BodyDef bd;
      bd.position.Set(-3.50f, 30.0f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(-3.50f, 30.0f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }
    
    
    //The sphere on it
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 1.0;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 30.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-3.50f, 31.5f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    /// # Another Balance See-Saw
     /*! 
     This is the see saw structure which is balanced on 
     * a traingular wedge and this is part of simulation 
     * when the ball falls on the right part of it ,this goes down and the 
     * other half goes up and hit the dominos and continue the simulation
     * and the process of feeding.
    */
    
    
    //balance 2
    {
                b2BodyDef *bd = new b2BodyDef;
      bd->type = b2_dynamicBody;
      bd->position.Set(-12,25.5);
      
      //The structure
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 100.0;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(6.0,0.1, b2Vec2(0.0f,0.0f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 0.01;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(0.1,1.5, b2Vec2(3.0f,1.5f), 0);
      fd2->shape = &bs2;
      b2FixtureDef *fd3 = new b2FixtureDef;
      fd3->density = 0.01;
      fd3->friction = 0.5;
      fd3->restitution = 0.f;
      fd3->shape = new b2PolygonShape;
      b2PolygonShape bs3;
      bs3.SetAsBox(0.1,1.5, b2Vec2(-3.0f,1.5f), 0);
      fd3->shape = &bs3;
      b2FixtureDef *fd4 = new b2FixtureDef;
      fd4->density = 0.01;
      fd4->friction = 0.5;
      fd4->restitution = 0.f;
      fd4->shape = new b2PolygonShape;
      b2PolygonShape bs4;
      bs4.SetAsBox(0.1,1.5, b2Vec2(6.0f,1.5f), 0);
      fd4->shape = &bs4;
      b2FixtureDef *fd5 = new b2FixtureDef;
      fd5->density = 0.01;
      fd5->friction = 0.5;
      fd5->restitution = 0.f;
      fd5->shape = new b2PolygonShape;
      b2PolygonShape bs5;
      bs5.SetAsBox(0.1,1.5, b2Vec2(-6.0f,1.5f), 0);
      fd5->shape = &bs5;
       
      b2Body* box1 = m_world->CreateBody(bd);
      box1->CreateFixture(fd1);
      box1->CreateFixture(fd2);
      box1->CreateFixture(fd3);
          box1->CreateFixture(fd4);
          box1->CreateFixture(fd5);        
                
                
                b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(-12.0f, 25.5f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = box1;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
                
                }
                
                
                
                {
                //The triangle wedge
      b2Body* sbody;
      b2PolygonShape poly;
      b2Vec2 vertices[3];
      vertices[0].Set(-1,0);
      vertices[1].Set(1,0);
      vertices[2].Set(0,1.5);
      poly.Set(vertices, 3);
      b2FixtureDef wedgefd;
      wedgefd.shape = &poly;
      wedgefd.density = 10.0f;
      wedgefd.friction = 0.0f;
      wedgefd.restitution = 0.0f;
      b2BodyDef wedgebd;
      wedgebd.position.Set(-12.0f, 23.85f);
      sbody = m_world->CreateBody(&wedgebd);
      sbody->CreateFixture(&wedgefd);


//The plank below wedge

      b2PolygonShape shape;
      shape.SetAsBox(6.0f, 0.2f);
      b2BodyDef bd2;
      bd2.position.Set(-12.0f, 23.5f);
      bd2.type = b2_staticBody;
      b2Body* body = m_world->CreateBody(&bd2);
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 1.f;
      fd2->shape = new b2PolygonShape;
      fd2->shape = &shape;
      body->CreateFixture(fd2);        
                        
                        
                        }
                        
   /// # Dominos 
     
      /*! 
     This shape represents a axis aligned box with width 0.1f and height
     * 1.0f and defines shape of dominos.
     * fd is a fixture definition with shape defined by dominos shape ,density
     * as 10.f.
    */
                         
    
       for (int i = 0; i < 8; ++i)
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 1.0f);
        
      b2BodyDef bd;
      bd.position.Set(-16.7f + i*1.0f, 29.5f + i*1.0f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(-16.7f + i*1.0f, 28.5f + i*1.0f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,-1);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }
    
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.1f, 2.0f);
        
      b2BodyDef bd;
      bd.position.Set(-16.7f + 8*1.0f, 39.0f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 10.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(-16.7f + 8*1.0f, 39.0f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }
    
    {
                b2PolygonShape shape;
      shape.SetAsBox(3.5f, 0.2f);
      b2BodyDef bd2;
      bd2.position.Set(-13.0f, 39.5f);
      bd2.type = b2_staticBody;
      b2Body* body = m_world->CreateBody(&bd2);
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 1.f;
      fd2->shape = new b2PolygonShape;
      fd2->shape = &shape;
      body->CreateFixture(fd2);

                
                }
                {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 1.0;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 1.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-10.2f, 40.8f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
                
    {
        b2PolygonShape shape;
      shape.SetAsBox(6.0f, 0.2f);
      b2BodyDef bd2;
      bd2.position.Set(-14.0f, 19.5f);
      bd2.angle=-0.1;
      bd2.type = b2_staticBody;
      b2Body* body = m_world->CreateBody(&bd2);
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 1.f;
      fd2->shape = new b2PolygonShape;
      fd2->shape = &shape;
      body->CreateFixture(fd2);
                
                }
    {
        b2PolygonShape shape;
      shape.SetAsBox(1.3f, 0.1f);
      b2BodyDef bd2;
      bd2.position.Set(-21.0f, 21.3f);
      bd2.angle=-1.0;
      bd2.type = b2_staticBody;
      b2Body* body = m_world->CreateBody(&bd2);
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 1.f;
      fd2->shape = new b2PolygonShape;
      fd2->shape = &shape;
      body->CreateFixture(fd2);
                
                }
     /// # Another Box with eatable 
      /*! 
     This is another open box containing eatable thing for dog which 
     * falls after the box is hitted by ball and the falling eatables 
     * falls in dog's box and feeds the dog.
    */
    
     
               
   //box2
    {
                b2BodyDef *bd = new b2BodyDef;
      bd->type = b2_dynamicBody;
      bd->position.Set(-5,17);
      
      //The open box
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 30.1;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(2.1,0.1, b2Vec2(0.0f,0.0f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 30.1;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(0.1,2.3, b2Vec2(2.0f,2.1f), 0);
      fd2->shape = &bs2;
      b2FixtureDef *fd3 = new b2FixtureDef;
      fd3->density = 30.1;
      fd3->friction = 0.5;
      fd3->restitution = 0.f;
      fd3->shape = new b2PolygonShape;
      b2PolygonShape bs3;
      bs3.SetAsBox(0.1,2.3, b2Vec2(-2.0f,2.1f), 0);
      fd3->shape = &bs3;
       
      b2Body* box1 = m_world->CreateBody(bd);
      box1->CreateFixture(fd1);
      box1->CreateFixture(fd2);
      box1->CreateFixture(fd3);
                
                b2PolygonShape shape2;
      shape2.SetAsBox(0.01f, 0.01f);
      b2BodyDef bd2;
      bd2.position.Set(-5.0f, 17.0f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = box1;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
                
                }  
   /// # Eatable in the Container
   
    /*! 
     This is the eatable things for dog which is made up of 
     * circles of radius 0.25f which falls from the container after 
     * inbalance of the shelf and goes to the dog's container.
    */
                 
                
   for (int i = -3; i <= 3; ++i){
    for (int j = 1; j <= 3; ++j)
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.25;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 4.0f;
      ballfd.friction = 0.1f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-5.0f+i*0.5f, 17.0f+j*0.5f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
}
     {
        b2PolygonShape shape;
      shape.SetAsBox(2.2f, 0.1f);
      b2BodyDef bd2;
      bd2.position.Set(-5.0f, 13.0f);
      bd2.angle=-0.1;
      bd2.type = b2_staticBody;
      b2Body* body = m_world->CreateBody(&bd2);
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 1.f;
      fd2->shape = new b2PolygonShape;
      fd2->shape = &shape;
      body->CreateFixture(fd2);
                
                }
 
   
   
   
   
   
   
   
   
   
    
    
    
    
    
    
    


    
    
    

    /*
    //The pulley system
    {
      b2BodyDef *bd = new b2BodyDef;
      bd->type = b2_dynamicBody;
      bd->position.Set(-9.6,12.0);
      bd->fixedRotation = true;

      //The open box
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 10.0;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(2,0.2, b2Vec2(0.f,-1.9f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 10.0;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(0.2,2, b2Vec2(2.0f,0.f), 0);
      fd2->shape = &bs2;
      b2FixtureDef *fd3 = new b2FixtureDef;
      fd3->density = 10.0;
      fd3->friction = 0.5;
      fd3->restitution = 0.f;
      fd3->shape = new b2PolygonShape;
      b2PolygonShape bs3;
      bs3.SetAsBox(0.2,2, b2Vec2(-2.0f,0.f), 0);
      fd3->shape = &bs3;

      b2Body* box1 = m_world->CreateBody(bd);
      box1->CreateFixture(fd1);
      box1->CreateFixture(fd2);
      box1->CreateFixture(fd3);

    }*/

    //The see-saw system at the bottom
 /*   {
      //The triangle wedge
      b2Body* sbody;
      b2PolygonShape poly;
      b2Vec2 vertices[3];
      vertices[0].Set(-1,0);
      vertices[1].Set(1,0);
      vertices[2].Set(0,1.5);
      poly.Set(vertices, 3);
      b2FixtureDef wedgefd;
      wedgefd.shape = &poly;
      wedgefd.density = 10.0f;
      wedgefd.friction = 0.0f;
      wedgefd.restitution = 0.0f;
      b2BodyDef wedgebd;
      wedgebd.position.Set(-23.0f, -3.0f);
      sbody = m_world->CreateBody(&wedgebd);
      sbody->CreateFixture(&wedgefd);


//The plank below wedge

      b2PolygonShape shape;
      shape.SetAsBox(9.0f, 0.2f);
      b2BodyDef bd2;
      bd2.position.Set(-23.0f, -2.0f);
      bd2.type = b2_staticBody;
      b2Body* body = m_world->CreateBody(&bd2);
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 1.f;
      fd2->shape = new b2PolygonShape;
      fd2->shape = &shape;
      body->CreateFixture(fd2);


    //The right upper system
   {
      b2BodyDef *bd = new b2BodyDef;
      bd->type = b2_dynamicBody;
      bd->position.Set(-30.0,0.0);
      bd->fixedRotation = true;

      //The open box
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 10.0;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(2,0.2, b2Vec2(0.f,-1.9f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 10.0;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(0.2,2, b2Vec2(2.0f,0.f), 0);
      fd2->shape = &bs2;
      b2FixtureDef *fd3 = new b2FixtureDef;
      fd3->density = 10.0;
      fd3->friction = 0.5;
      fd3->restitution = 0.f;
      fd3->shape = new b2PolygonShape;
      b2PolygonShape bs3;
      bs3.SetAsBox(0.2,2, b2Vec2(-2.0f,0.f), 0);
      fd3->shape = &bs3;

      b2Body* box1 = m_world->CreateBody(bd);
      box1->CreateFixture(fd1);
      box1->CreateFixture(fd2);
      box1->CreateFixture(fd3);

    }


 }

 //The see-saw system at the Upper part
    {
      //The triangle wedge
      b2Body* sbody;
      b2PolygonShape poly;
      b2Vec2 vertices[3];
      vertices[0].Set(-1,0);
      vertices[1].Set(1,0);
      vertices[2].Set(0,1.5);
      poly.Set(vertices, 3);
      b2FixtureDef wedgefd;
      wedgefd.shape = &poly;
      wedgefd.density = 10.0f;
      wedgefd.friction = 0.0f;
      wedgefd.restitution = 0.0f;
      b2BodyDef wedgebd;
      wedgebd.position.Set(-10.0f, 19.0f);
      sbody = m_world->CreateBody(&wedgebd);
      sbody->CreateFixture(&wedgefd);

//The plank on bottom of the wedge

      b2PolygonShape shape;
      shape.SetAsBox(3.0f, 0.2f);
      b2BodyDef bd2;
      bd2.position.Set(-10.0f, 20.8f);
      bd2.type = b2_staticBody;
      b2Body* body = m_world->CreateBody(&bd2);
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 1.f;
      fd2->shape = new b2PolygonShape;
      fd2->shape = &shape;
      body->CreateFixture(fd2);

 }

  //The triangle wedge
 {
      b2Body* sbody;
      b2PolygonShape poly;
      b2Vec2 vertices[3];
      vertices[0].Set(-1,0);
      vertices[1].Set(1,0);
      vertices[2].Set(0,1.5);
      poly.Set(vertices, 3);
      b2FixtureDef wedgefd;
      wedgefd.shape = &poly;
      wedgefd.density = 10.0f;
      wedgefd.friction = 0.0f;
      wedgefd.restitution = 0.0f;
      b2BodyDef wedgebd;
      wedgebd.position.Set(-9.6f, 9.0f);
      sbody = m_world->CreateBody(&wedgebd);
      sbody->CreateFixture(&wedgefd);
      }
      
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 1.5;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 50.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_staticBody;
      ballbd.position.Set(-10.0f, 22.3f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    
  */  
    
    
    
    
   
   /// # Revolving  horizontal platform
    /*! 
     shape is a axis aligned box with
     * length 2.2f and width 0.2f.
     * bd is dynamic body definition with coordinates as (-27f,10.4f)
     * body is rigid body created using definiton bd* and is
     *  attached to the fixture fd. 
     * fd defines a fixture definition
     *  with density 1f and using shape defined above.
     * It contains a sphere of radius 1.0f before start revolving.
     * It starts revolving after hitted by a box. 
    */
    


 //The revolving horizontal platform
    {
      b2PolygonShape shape;
      shape.SetAsBox(2.2f, 0.2f);
        
      b2BodyDef bd;
      bd.position.Set(-27.0f, 10.4f);
      bd.type = b2_dynamicBody;
      b2Body* body = m_world->CreateBody(&bd);
      b2FixtureDef *fd = new b2FixtureDef;
      fd->density = 1.f;
      fd->shape = new b2PolygonShape;
      fd->shape = &shape;
      body->CreateFixture(fd);

      b2PolygonShape shape2;
      shape2.SetAsBox(0.2f, 0.2f);
      b2BodyDef bd2;
      bd2.position.Set(-27.0f, 12.4f);
      b2Body* body2 = m_world->CreateBody(&bd2);

      b2RevoluteJointDef jointDef;
      jointDef.bodyA = body;
      jointDef.bodyB = body2;
      jointDef.localAnchorA.Set(0,0);
      jointDef.localAnchorB.Set(0,0);
      jointDef.collideConnected = false;
      m_world->CreateJoint(&jointDef);
    }

    //The heavy sphere on the platform
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 1.0;
        
      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 50.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-27.0f, 13.5f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }

    
   
{
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-40.5f, 9.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
{
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-34.5f, 9.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.05f);
        
      b2BodyDef bd;
      bd.position.Set(-43.0f, 6.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
     /// # See-Saw System 
    /*! 
     This is the see-saw.system at the left buttom side it contains 
     * one open box at left side and a close box at right side.
     * The balls falling from ball bearing falls in the laft box nad it goes 
     * down and lifts the right half which hits a ball and continues 
     * simulation. 
    */
    
    
      //The see-saw system at the bottom
  {
      //The triangle wedge
      b2Body* sbody;
      b2PolygonShape poly;
      b2Vec2 vertices[3];
      vertices[0].Set(-1,0);
      vertices[1].Set(1,0);
      vertices[2].Set(0,1.5);
      poly.Set(vertices, 3);
      b2FixtureDef wedgefd;
      wedgefd.shape = &poly;
      wedgefd.density = 10.0f;
      wedgefd.friction = 0.0f;
      wedgefd.restitution = 0.0f;
      b2BodyDef wedgebd;
      wedgebd.position.Set(-35.0f, 7.0f);
      sbody = m_world->CreateBody(&wedgebd);
      sbody->CreateFixture(&wedgefd);




    //The right upper system
    {
      b2BodyDef *bd = new b2BodyDef;
      bd->type = b2_dynamicBody;
      bd->position.Set(-43.0,11.0);
      //bd->fixedRotation = true;

      //The open box
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 10.0;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(2,0.2, b2Vec2(0.f,-2.9f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 10.0;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(0.2,3, b2Vec2(2.0f,0.f), 0);
      fd2->shape = &bs2;
      b2FixtureDef *fd3 = new b2FixtureDef;
      fd3->density = 10.0;
      fd3->friction = 0.5;
      fd3->restitution = 0.f;
      fd3->shape = new b2PolygonShape;
      b2PolygonShape bs3;
      bs3.SetAsBox(0.2,3, b2Vec2(-2.0f,0.f), 0);
      fd3->shape = &bs3;
      b2FixtureDef *fd4 = new b2FixtureDef;
      fd4->density = 10.0;
      fd4->friction = 0.5;
      fd4->restitution = 0.f;
      fd4->shape = new b2PolygonShape;
      b2PolygonShape bs4;
      bs4.SetAsBox(8,0.1, b2Vec2(6.f,-2.9f), 0);
      fd4->shape = &bs4;
      b2FixtureDef *fd5 = new b2FixtureDef;
      fd5->density = 22.0;
      fd5->friction = 0.5;
      fd5->restitution = 0.f;
      fd5->shape = new b2PolygonShape;
      b2PolygonShape bs5;
      bs5.SetAsBox(1.0,1.0, b2Vec2(15.f,-1.9f), 0);
      fd5->shape = &bs5;

      b2Body* box1 = m_world->CreateBody(bd);
      box1->CreateFixture(fd1);
      box1->CreateFixture(fd2);
      box1->CreateFixture(fd3);
      box1->CreateFixture(fd4);

      box1->CreateFixture(fd5);
                


    }
    
    
    
    
    }
    
    /// # Slaint Horizontal Shelf
   /*!
    * This the right slaint horizontal shape of
    *  length 6.0f and width 0.25f.It helps in falling the eatable in the dog's box. 
    */
  // right slaint horizontal shelf
    {
      b2PolygonShape shape;
      shape.SetAsBox(6.0f, 0.25f);

      b2BodyDef bd;
      bd.angle=2.08;
      bd.position.Set(1.2f, 8.4f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    {
      b2PolygonShape shape;
      shape.SetAsBox(2.6f, 0.2f);

      b2BodyDef bd;
      bd.angle=0.1;
      bd.position.Set(10.8f, 3.1f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
{
      b2PolygonShape shape;
      shape.SetAsBox(0.05f, 0.5f);

      b2BodyDef bd;
      bd.position.Set(13.4f, 4.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    
    
    /// # The Dog
    /*!
     * The Dog is the pet animal which is made from two
     * circle one of radius 1.8f and another of radius 2.8f
     * with two legs made from two slaint horizontal line of length 1.0f
     * and width 0.20f.
     */ 
// The dog


    //The heavy sphere on the platform
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 1.8;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 50.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_staticBody;
      ballbd.position.Set(0.0f, 3.5f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
}{
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 2.8;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 50.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_staticBody;
      ballbd.position.Set(-4.5f, 2.2f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }

    // right slaint horizontal shelf
    {
      b2PolygonShape shape;
      shape.SetAsBox(1.0f, 0.20f);

      b2BodyDef bd;
      bd.angle=100;
      bd.position.Set(-2.0f, -1.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }

    {
      b2PolygonShape shape;
      shape.SetAsBox(1.0f, 0.20f);

      b2BodyDef bd;
      bd.angle=10;
      bd.position.Set(-7.0f, -1.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    /// # The Dog's Box
    /*!
     * This is the dog's box in which we are feeding the dog. 
     */
    // The dog cup
    {
      b2BodyDef *bd = new b2BodyDef;
      bd->type = b2_staticBody;
      bd->position.Set(6.0,0.0);
      bd->fixedRotation = true;

      //The open box
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 10.0;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(2,0.2, b2Vec2(0.f,-2.9f), 0);
      fd1->shape = &bs1;
      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 10.0;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(0.2,3, b2Vec2(2.0f,0.f), 0);
      fd2->shape = &bs2;
      b2FixtureDef *fd3 = new b2FixtureDef;
      fd3->density = 10.0;
      fd3->friction = 0.5;
      fd3->restitution = 0.f;
      fd3->shape = new b2PolygonShape;
      b2PolygonShape bs3;
      bs3.SetAsBox(0.2,3, b2Vec2(-2.0f,0.f), 0);
      fd3->shape = &bs3;

      b2Body* box1 = m_world->CreateBody(bd);
      box1->CreateFixture(fd1);
      box1->CreateFixture(fd2);
      box1->CreateFixture(fd3);

    }
   
   
   
   
   
   
   
   
   
   
   
  }

  sim_t *sim = new sim_t("Dominos", dominos_t::create);
}
